# CRUD with Flask - Docker - PostgreSQL

## Building & Running

### Clone the project
```

git clone https://github.com/kenter-vectorive/CRUD-Flask-Docker-Postgres.git

```

### Build and run it on a desktop

```

docker-compose up --build

```


